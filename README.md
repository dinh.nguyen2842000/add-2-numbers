Source code project gồm 2 file: Main.java, MyBigNumber.java.
Nằm trong thư mục src


Clone project từ gitlab về máy

```bash
git clone https://gitlab.com/dinh.nguyen2842000/add-2-numbers.git
```

## Chạy chương trình

```bash
> cd <project-folder>/src

# Biên dịch chương trình
> javac Main.java

# Chạy chương trình
> java Main
```
