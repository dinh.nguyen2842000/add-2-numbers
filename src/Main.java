import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        MyBigNumber myBigNumber = new MyBigNumber();

        Logger logger = Logger.getLogger("logName");

        String a = "123445";
        String b = "6246";
        logger.log(Level.INFO, myBigNumber.sum(a, b));
    }
}