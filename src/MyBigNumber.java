public class MyBigNumber {
    public String sum(String stn1, String stn2) {
        String result = "";
        int check = 0;

        while (stn1.length() < stn2.length())
            stn1 = "0" + stn1;
        while (stn2.length() < stn1.length())
            stn2 = "0" + stn2;

        for (int i = stn1.length(); i > 0; i--) {

            int digitStn1 = Character.getNumericValue(stn1.charAt(i - 1));

            int digitStn2 = Character.getNumericValue(stn2.charAt(i - 1));

            int tmp = digitStn1 + digitStn2 + check;

            check = tmp / 10;

            int digitNumber = tmp % 10;

            result = digitNumber + result;

        }

        if (check == 1) {
            result = "1" + result;
        }

        return result;

    }
}
